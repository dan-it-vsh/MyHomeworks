function checkAge(age) {
    while (isNaN(Number(age)) || Number(age) < 10 || Number(age) > 100) {
        age = prompt("Please enter correct age, should be a number between 10 and 100 : ", String(age));
    }
    return age;
}

function checkName(name) {
    while ( name === null || name.trim().length === 0 || !isNaN(parseFloat(name)) ) {
        name = prompt("Please enter correct name, should be a not empty string or number: ", String(name));
    }
    return name.trim();
}

let name = prompt("Enter your name:", "");
name = checkName(name);

let age = prompt("Enter your age:", "");
age = checkAge(age);

if (Number(age) < 18) {
    alert("You are not allowed to visit this website");
}

if (Number(age) >= 18 && Number(age) <=22) {
    if (confirm("Are you sure you want to continue?")) {
        alert("Welcome " + name[0].toUpperCase() + name.slice(1, name.length).toLowerCase() + "!");
    }
    else {
        alert("You are not allowed to visit this website");
    }
}

if (Number(age) > 22) {
    alert("Welcome " + name[0].toUpperCase() + name.slice(1, name.length).toLowerCase() + "!");
}