function funcCloneObject(obj) {
    let copiedObject;

        // Process simple types
        if (null == obj || "object" != typeof obj) return obj;

        // Process Array
        if (obj instanceof Array) {
            copiedObject = [];
            for (let i = 0, len = obj.length; i < len; i++) {
                copiedObject[i] = funcCloneObject(obj[i]);
            }
            return copiedObject;
        }

        // Process Object
        if (obj instanceof Object) {
            copiedObject = {};
            for (let attr in obj) {
                if (obj.hasOwnProperty(attr)) copiedObject[attr] = funcCloneObject(obj[attr]);
            }
            return copiedObject;
        }
        return false;
}

let sourceObject = {
    "name": "Petrov",
    "age": 28,
    "aaa": null,
    "colorArray": ["blue", "green", "yellow"],
    "customer": {
        "customerFirstName": "Firstname",
        "customerLastName": "Lastnname",
        "customerCitizenship": "aborigen",
        "customerSets": ["set1", "set2", 4, "set53"],
    }
}

let clonedObject = {};
clonedObject = funcCloneObject(sourceObject);

console.log(sourceObject);
console.log(clonedObject);
console.log(funcCloneObject("lalalala"));
console.log(funcCloneObject(["yellow", "sub", "marine"]));
console.log(funcCloneObject(5555));
console.log(funcCloneObject('S'));
console.log(funcCloneObject(null));
console.log(funcCloneObject(undefined));
