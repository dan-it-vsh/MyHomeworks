function excludeBy(arrSource, arrExcludedFrom, propertyExcluded) {
    let newArr = [];
    for (let i = 0, k = 0; i < arrSource.length; i++) {
        isInExcludedFrom = 0;
        for (let j = 0; j < arrExcludedFrom.length; j++) {
            if (arrSource[i][propertyExcluded] === arrExcludedFrom[j][propertyExcluded]) {
                isInExcludedFrom = 1;
                break;
            }
        }
        if (!isInExcludedFrom) newArr[k++] = funcCopyObject(arrSource[i]);
    }
    return newArr;
}

function funcCopyObject(obj) {
    let copiedObject;

    if (null == obj || "object" != typeof obj) return obj;

    if (obj instanceof Object) {
        copiedObject = {};
        for (let attr in obj) {
            if (obj.hasOwnProperty(attr)) copiedObject[attr] = funcCopyObject(obj[attr]);
        }
        return copiedObject;
    }
    return false;
}

const usersList = [
    {
        name: "Ivan",
        surname: "Ivanov",
        gender: "male",
        age: 30
    },
    {
        name: "Vasia",
        surname: "Sidorov",
        gender: "male",
        age: 40
    },
    {
        name: "Anna",
        surname: "Ivanova",
        gender: "female",
        age: 22
    },
    {
        name: "Vasilisa",
        surname: "Karpova",
        gender: "female",
        age: 24
    }];

const usersListExcluded = [
    {
        name: "Ivan",
        surname: "Petrov",
        gender: "male",
        age: 29
    },
    {
        name: "Nicole",
        surname: "Ivanova",
        gender: "female",
        age: 23
    },
    {
        name: "Vasia",
        surname: "Pupkin",
        gender: "male",
        age: 24
    }];


console.log(excludeBy(usersList, usersListExcluded, 'name'));
console.log(excludeBy(usersList, usersListExcluded, 'surname'));
console.log(excludeBy(usersList, usersListExcluded, 'age'));
