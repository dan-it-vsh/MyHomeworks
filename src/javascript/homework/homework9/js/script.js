function funcZodiacSign(day, month) {
    let zodSigns = [
        "Capricorn",
        "Aquarius",
        "Pisces",
        "Aries",
        "Taurus",
        "Gemini",
        "Cancer",
        "Leo",
        "Virgo",
        "Libra",
        "Scorpio",
        "Sagittarius"
    ];
    let zodiacSign = "";
    switch (month) {
        case 1: {//January
            if (day < 20)
                zodiacSign = zodSigns[0];
            else
                zodiacSign = zodSigns[1];
        }
            break;
        case 2: {//February
            if (day < 19)
                zodiacSign = zodSigns[1];
            else
                zodiacSign = zodSigns[2];
        }
            break;
        case 3: {//March
            if (day < 21)
                zodiacSign = zodSigns[2];
            else
                zodiacSign = zodSigns[3];
        }
            break;
        case 4: {//April
            if (day < 20)
                zodiacSign = zodSigns[3];
            else
                zodiacSign = zodSigns[4];
        }
            break;
        case 5: {//May
            if (day < 21)
                zodiacSign = zodSigns[4];
            else
                zodiacSign = zodSigns[5];
        }
            break;
        case 6: {//June
            if (day < 21)
                zodiacSign = zodSigns[5];
            else
                zodiacSign = zodSigns[6];
        }
            break;
        case 7: {//July
            if (day < 23)
                zodiacSign = zodSigns[6];
            else
                zodiacSign = zodSigns[7];
        }
            break;
        case 8: {//August
            if (day < 23)
                zodiacSign = zodSigns[7];
            else
                zodiacSign = zodSigns[8];
        }
            break;
        case 9: {//September
            if (day < 23)
                zodiacSign = zodSigns[8];
            else
                zodiacSign = zodSigns[9];
        }
            break;
        case 10: {//October
            if (day < 23)
                zodiacSign = zodSigns[9];
            else
                zodiacSign = zodSigns[10];
        }
            break;
        case 11: {//November
            if (day < 22)
                zodiacSign = zodSigns[10];
            else
                zodiacSign = zodSigns[11];
        }
            break;
        case 12: {//December
            if (day < 22)
                zodiacSign = zodSigns[11];
            else
                zodiacSign = zodSigns[0];
        }
            break;
    }
    return zodiacSign;
}

function funcYearAnimal(yyyy) {
    let animals = ["Monkey", "Cock", "Dog", "Boar", "Rat", "Ox", "Tiger", "Rabbit", "Dragon", "Snake", "Horse", "Sheep"];
    return animals[yyyy % 12];
}


function funcGetAge(dd, mm, yyyy) {
    let today = new Date();
    let age = today.getFullYear() - yyyy;
    if (today.getMonth() < mm || (today.getMonth() === mm && today.getDate() < dd)) {
        age--;
    }
    return age;
}

function funcValidateDate(inputDate) {
    let dateFormat = /^(0?[1-9]|[12][0-9]|3[01]).(0?[1-9]|1[012]).\d{4}$/;
    let dateIsCorrect = 0;
    while (!dateIsCorrect) {
        if (inputDate === "" || inputDate === null) {
            inputDate = prompt("Please enter the date like 31.03.1994 :", String(inputDate));
            continue;
        }
        if (!inputDate.match(dateFormat)) {
            inputDate = prompt("Date format is wrong, please enter correct date like 15.04.2007 :", String(inputDate));
            continue;
        }
        else {
            let date = inputDate.split('.');
            let dd = parseInt(date[0]);
            let mm = parseInt(date[1]);
            let yyyy = parseInt(date[2]);
            let listOfDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
            if (mm === 1 || mm > 2) {
                if (dd > listOfDays[mm - 1]) {
                    inputDate = prompt("Date format is wrong, please enter correct date :", String(inputDate));
                    continue;
                }
            }
            if (mm === 2) {
                let isLeapYear = false;
                if ((!(yyyy % 4) && yyyy % 100) || !(yyyy % 400)) {
                    isLeapYear = true;
                }
                if ((isLeapYear === false) && (dd >= 29)) {
                    inputDate = prompt("Date format is wrong, please enter correct date :", String(inputDate));
                    continue;
                }
                if ((isLeapYear === true) && (dd > 29)) {
                    inputDate = prompt("Date format is wrong, please enter correct date :", String(inputDate));
                    continue;
                }
            }
        }
        dateIsCorrect = 1;
    }
    return inputDate;
}

let userBirthDateInput = funcValidateDate(prompt("Enter your birth date in format dd.mm.yyyy", ""));
let userBirthDate = userBirthDateInput.split('.');
let dd = parseInt(userBirthDate[0]);
let mm = parseInt(userBirthDate[1]);
let yyyy = parseInt(userBirthDate[2]);

alert("You are " + funcGetAge(dd, mm, yyyy) + " years old.");
alert("Your Zodiac Sign is : " + funcZodiacSign(dd, mm) + ".");
alert("Your Chinese calendar animal is : " + funcYearAnimal(yyyy) + ".");