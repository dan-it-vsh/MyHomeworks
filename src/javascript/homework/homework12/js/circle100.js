function drawCircles(diameter) {
    if (isNaN(Number(diameter))) return false;

    let canvasWidth = +diameter + +2;
    let canvasHeight = +diameter + +2;
    let centerX = parseInt(canvasWidth / 2, 10);
    let centerY = parseInt(canvasHeight / 2, 10);

    let container = document.createElement("DIV");
    container.style.cssText = "display: flex; " +
        "flex-wrap: wrap; " +
        "justify-content: space-between; " +
         "width: " + canvasWidth * 10 + "px;";
        document.body.appendChild(container);

    for (i = 0; i < 100; i++) {
        let canvas = document.createElement("CANVAS");
        canvas.setAttribute("width", `${canvasWidth}px`);
        canvas.setAttribute("height", `${canvasHeight}px`);
        canvas.style.cssText = "display: block; " +
            "margin: auto;" +
            "flex: 0 1 auto;";
        container.appendChild(canvas);

        let colorR = Math.floor(Math.random() * (256));
        let colorG = Math.floor(Math.random() * (256));
        let colorB = Math.floor(Math.random() * (256));
        let color = `rgba(${colorR}, ${colorG}, ${colorB})`;

        let context = canvas.getContext('2d');
        context.beginPath();
        context.arc(centerX, centerY, diameter / 2, 0, 2 * Math.PI, false);
        context.fillStyle = color;
        context.fill();
        context.lineWidth = 1;
        context.strokeStyle = color;
        context.stroke();
    }
    return true;
}

let btnStart = document.getElementById("myBtnStart");
btnStart.style.cssText = 'display: block;';

btnStart.onclick = function () {
    btnStart.style.cssText = 'display: none;';
    let drawBtnIsClickedOnce = 0;

    // create input for diameter
    let inputDiameter = document.createElement("INPUT");
    inputDiameter.setAttribute("type", "text");
    inputDiameter.setAttribute("placeholder", "Diameter of the circle in px");
    document.body.appendChild(inputDiameter);
    let inputDiameterTextAfter = document.createElement("P");
    inputDiameterTextAfter.style.cssText = 'display: inline-block; margin: 0 0 0 5px;';
    inputDiameterTextAfter.appendChild(document.createTextNode("px"));
    document.body.appendChild(inputDiameterTextAfter);

    // button "Draw!!!"
    let btn = document.createElement("BUTTON");
    btn.setAttribute("type", "submit");
    btn.setAttribute("id", "myBtnDraw");
    document.body.appendChild(document.createElement("BR"));
    btn.appendChild(document.createTextNode("Draw!"));
    btn.style.cssText = 'font-size: 16px; padding: 10px 25px; color: red;';
    btn.onclick = function () {
        if (!inputDiameter.value) inputDiameter.value = 50;
        if (!drawBtnIsClickedOnce) drawCircles(Number(inputDiameter.value));
        drawBtnIsClickedOnce = 1;
    };
    document.body.appendChild(btn);
};

document.addEventListener("click", function () {
    let elemList = document.querySelectorAll(':hover');
    [].forEach.call(elemList, function (elem) {
        console.log(elem.nodeName);
        if (elem.nodeName === "CANVAS") {
            elem.style.cssText += 'opacity: 0;';
        }
    });
});