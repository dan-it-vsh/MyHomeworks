function drawACircle(diameter, color) {
    if (isNaN(Number(diameter))) return false;
    if (!toString(color)) return false;

    let canvasWidth = +diameter + +20;
    let canvasHeight = +diameter + +20;
    let centerX = parseInt(canvasWidth / 2, 10);
    let centerY = parseInt(canvasHeight / 2, 10);

    let canvas = document.createElement("CANVAS");
    canvas.setAttribute("width", `${canvasWidth}px`);
    canvas.setAttribute("height", `${canvasHeight}px`);

    document.body.appendChild(document.createElement("BR"));
    document.body.appendChild(canvas);

    let context = canvas.getContext('2d');
    context.beginPath();
    context.arc(centerX, centerY, diameter / 2, 0, 2 * Math.PI, false);
    context.fillStyle = color;
    context.fill();
    context.lineWidth = 1;
    context.strokeStyle = color;
    context.stroke();
    return true;
}

let btnStart = document.getElementById("myBtnStart");
btnStart.style.cssText = 'display: block;';

btnStart.onclick = function () {
    btnStart.style.cssText = 'display: none;';
    let drawBtnIsClickedOnce = 0;

    // create input for diameter
    let inputDiameter = document.createElement("INPUT");
    inputDiameter.setAttribute("type", "text");
    inputDiameter.setAttribute("placeholder", "Diameter of the circle in px");
    document.body.appendChild(inputDiameter);
    let inputDiameterTextAfter = document.createElement("P");
    inputDiameterTextAfter.style.cssText = 'display: inline-block; margin: 0 0 0 5px;';
    inputDiameterTextAfter.appendChild(document.createTextNode("px"));
    document.body.appendChild(inputDiameterTextAfter);

    // create input for color
    let inputColor = document.createElement("INPUT");
    inputColor.setAttribute("type", "text");
    inputColor.setAttribute("placeholder", "Color of the circle");
    document.body.appendChild(document.createElement("BR"));
    document.body.appendChild(inputColor);
    let inputColorTextAfter = document.createElement("P");
    inputColorTextAfter.style.cssText = 'display: inline-block; margin: 0 0 0 5px;';
    inputColorTextAfter.appendChild(document.createTextNode("RGB, HEX, HSL"));
    document.body.appendChild(inputColorTextAfter);

    // button "Draw!!!"
    let btn = document.createElement("BUTTON");
    btn.setAttribute("type", "submit");
    btn.setAttribute("id", "myBtnDraw");
    document.body.appendChild(document.createElement("BR"));
    btn.appendChild(document.createTextNode("Draw!"));
    btn.style.cssText = 'font-size: 16px; padding: 10px 25px; color: red;';
    btn.onclick = function () {
        if (!inputDiameter.value) inputDiameter.value = 50;
        if (!inputColor.value) inputColor.value = "#555555";
        if (!drawBtnIsClickedOnce)
            drawACircle(Number(inputDiameter.value), `${inputColor.value}`);
        drawBtnIsClickedOnce = 1;
    };
    document.body.appendChild(btn);
};