function checkNumber(number) {
    while (!Number.isInteger(number)) {
        number = Number(prompt("Enter correct integer number (e.g. 17): ", ""));
    }
    return Number(number);
}

function funcFib(number) {
    let a = 0, b = 1, c;
    if ( number === 0 ) return 0;
    if ( number > 0 ) {
        for (let i = 2; i <= number; i++) {
            c = a + b;
            a = b;
            b = c;
        }
    }
    if ( number < 0 ) {
        for (let i = -2; i >= number; i--) {
            c = a - b;
            a = b;
            b = c;
        }
    }
    return b;
}

let number = Number(prompt("Enter integer number : ", ""));
number = checkNumber(number);
alert("Fibonacci number (" + number + ") = " + funcFib(number));
