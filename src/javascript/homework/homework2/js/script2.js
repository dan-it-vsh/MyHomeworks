function checkNumbersRange(m, n) {
    while (!Number.isInteger(m) || m <= 1 || !Number.isInteger(n) || n <= 1 || n <= m) {
        alert("Incorrect range numbers. Must be integer values > 1 and m > n ");
        m = Number(prompt("Enter correct integer number m > 1 (e.g. 34): ", ""));
        if (isNaN(m) || m <= 1) {
            continue;
        }
        else {
            n = Number(prompt("Enter correct integer number n > " + m + " (e.g. " + String(Number(m) + 23) + "): ", ""));
        }
    }
    return [m, n];
}

function checkIsPrimeNumber(number) {
    for (let i = 2; i < number; i++) {
        if (number % i === 0) {
            return false;
        }
    }
    return true;
}

/* Sub-task 2 */

let rangeNumbers = checkNumbersRange(Number(prompt("Enter integer number m > 1 : ", "")),
    Number(prompt("Enter integer number n > m : ", "")));
arrayPrimeNumbers = [];
for (let i = rangeNumbers[0]; i <= rangeNumbers[1]; i++) {
    if (checkIsPrimeNumber(i)) {
        console.log(i);
    }
}
