function checkNumber(number) {
    while (!Number.isInteger(number) || number <= 1) {
        number = Number(prompt("Enter correct integer number > 1 (e.g. 34): ", ""));
    }
    return Number(number);
}

function checkIsPrimeNumber(number) {
    for (let i = 2; i < number; i++) {
        if (number % i === 0) {
            return false;
        }
    }
    return true;
}

let number = Number(prompt("Enter integer number > 1 : ", ""));
let arrayPrimeNumbers = [];
number = checkNumber(number);
for (let i = 1; i <= number; i++) {
    if (checkIsPrimeNumber(i)) {
        console.log(i);
    }
}