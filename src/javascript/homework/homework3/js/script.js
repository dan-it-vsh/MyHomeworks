function checkNumber(number) {
    while (!Number.isInteger(number) || number < 1) {
        number = Number(prompt("Enter correct integer number >= 1 (e.g. 34): ", ""));
    }
    return Number(number);
}

function funcFactorial(number) {
    if (number > 1) {
        return number * funcFactorial(number - 1);
    }
    else return 1;
}

let number = Number(prompt("Enter integer number >= 1 : ", ""));
number = checkNumber(number);

console.log(funcFactorial(number));
