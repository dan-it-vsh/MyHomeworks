function StopWatch() {
    let times = [0, 0, 0];
    let time = null;
    let isRunning = false;
    let display = document.querySelector('.stopwatch');
    let self = this;
    display.innerText = "00:00:000";

    this.toggleStartPause = function () {
        if (!time) time = performance.now();
        if (!isRunning) {
            isRunning = true;
            document.getElementById("myStartBtn").innerText = "Pause";
            requestAnimationFrame(self.step.bind(self));
        } else {
            isRunning = false;
            document.getElementById("myStartBtn").innerText = "Start";
            requestAnimationFrame(self.step.bind(self));
            time = null;
        }
    };

    this.clear = function () {
        if (isRunning) isRunning = false;
            times = [0, 0, 0];
            time = null;
            display.innerText = "00:00:000";
            requestAnimationFrame(self.step.bind(self));

    };

    this.step = function (timestamp) {
        if (!isRunning) return;
        self.calculate(timestamp);
        time = timestamp;
        self.print();
        requestAnimationFrame(self.step.bind(self));
    };

    this.calculate = function (timestamp) {
        times[2] += timestamp - time;
        console.log(times[2]);

        // Seconds are 1000 msec
        if (times[2] >= 1000) {
            times[1] += 1;
            times[2] -= 1000;
        }
        // Minutes are 60 seconds
        if (times[1] >= 60) {
            times[0] += 1;
            times[1] -= 60;
        }
    };

    this.print = function () {
        display.innerText = self.formatTime(times);
    };

    this.formatTime = function (x) {
        return `\
${self.padding(x[0], 2)}:\
${self.padding(x[1], 2)}:\
${self.padding(Math.floor(x[2]), 3)}`;
    };

    this.padding = function (value, count) {
        let result = value.toString();
        for (; result.length < count; --count)
            result = '0' + result;
        return result;
    }
}

let stopWatch = new StopWatch();
