let modal = document.getElementById('myModal');

modal.style.display = "block";

let timeLeft = 10;
let downTimer = setInterval(function () {
    document.getElementById("timer").innerHTML = --timeLeft;
    if (timeLeft <= 0) {
        clearInterval(downTimer);
        document.getElementById("timer").innerHTML = "";
        modal.style.display = "none";
    }
}, 1000);
