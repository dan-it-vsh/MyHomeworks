function funcValidateLiNumber(number) {
    while (isNaN(number) || number <= 0 || number > 10) {
        if (number > 10) {
            number = Number(prompt("Isn't it too much? Enter a smaller number of list items between 1 and 10: ", String(number)));
            continue;
        }
        number = Number(prompt("Enter correct number of list items >= 1: ", String(number)));
    }
    return number;
}

let liNumber = funcValidateLiNumber(Number(prompt("Enter number of list items >= 1: ", "")));
let liItems = [];

for (let i=0; i < liNumber; i++) {
    liItems[i] = prompt(`Enter list item ${i + 1} value as string : `, "");
}

liItems.map(function(item) {
    let node = document.createElement('li');
    node.appendChild(document.createTextNode(item));
    document.getElementById("list").appendChild(node)
});