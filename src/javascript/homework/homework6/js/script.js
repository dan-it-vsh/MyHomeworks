function CreateNewUser() {
    this.firstName = prompt("Enter First Name : ", "");
    this.lastName = prompt("Enter Last Name : ", "");
    return this;
}

let userCurrent = new CreateNewUser();

CreateNewUser.prototype.getLogin = function () {
    return this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase();
};

console.log(userCurrent.getLogin());
console.log(userCurrent);

CreateNewUser.prototype.setFirstName = function (someFirstName) {
    this.firstName = someFirstName;
};

CreateNewUser.prototype.setLastName = function (someLastName) {
    this.lastName = someLastName;
};

userCurrent.setFirstName("Petia");
userCurrent.setLastName("Vasechkin");
console.log(userCurrent.getLogin());
console.log(userCurrent);

