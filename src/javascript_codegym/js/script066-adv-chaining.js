function plus(value) {
    if (typeof this.result !== "underfined") {
        this.result = +value;
    }
    else {
        this.result += +value;
    }
    console.log(this.result);
    return this;
}

function minus(value) {
    this.result -= +value;
    return this;
}

// function cloneArray(arr) {
//     let newArr = arr.slice(0, arr.length);
//     return newArr;
// }
// function titleCase(str) {
//         let res = "";
//         str = str.split(" ");
//         for (let i in str) {
//             res += str[i].substring(0,1).toUpperCase() + str[i].substring(1,).toLowerCase() +" ";
//         }
// return res;
// }

// plus(20).minus(1).plus(6).result
// var arr = [1,2,3];
// var newArr = cloneArray(arr);
// newArr[0] = 4;
// console.log(newArr);

// console.log(titleCase("hhhhhh HHHHHH"));
console.log(plus(20).minus(1).plus(6).result);
console.log(plus(20).minus(1).plus(6).plus(10).minus(40).result);

function addToStack(arr, item) {
    arr.push(item);
    arr.splice(0,1);
    return arr;

}

console.log(addToStack([1, 2, 3], 4));
console.log(addToStack([8,9,10], 11));

function contains(str, subString) {
  if (str.toLowerCase().indexOf(subString.toLowerCase()) !== -1) {
    return true;
  }
  else return false;
}

console.log(contains("Hello World!", "world"));

function insertionSort(arr) {
    for (let i = 0; i < arr.length; i++) {
        for (let j = i + 1; j < arr.length ; j++) {
            if (arr[j] < arr[i]) {
                let tempVar = arr[i];
                arr[i] = arr[j];
                arr[j] = tempVar;
            }
        }
    }
    return arr;
}

console.log(insertionSort([2, 4, -5, 0]));

//prompt user to enter a number to calculate the factorial
// var num = prompt('What number do you want to find the factorial of? ');

//recursive
function factorial(n) {
    if(n == 0) {
        return 1
    } else {
        return n * factorial(n - 1);
    }
}

console.log(factorial(7));

String.prototype.subSet = function () {
    let arr = [];
    for (i = 0; i < this.length; i++) {
        for (j = i + 1; j <= this.length ; j++) {
            arr.push(this.slice(i, j));
        }
    }
    return arr;
};

console.log("Hello".subSet());
console.log("cat".subSet());

function countVowels(str) {
    let count = 0;
    str = str.toLowerCase();
    let vowels = ["a", "e", "i", "o", "u"];
    for (let i in str) {
        // if (str[i] == "a" || str[i] == "e" || str[i] == "i" || str[i] == "o" || str[i] == "u") {
        if (vowels.includes(str[i])) {
            count++;
        }
    }
    return count;
}
console.log(countVowels("Maintenance"));

function isDaylightsSavings(date) {
    date = String(date);
    if (date.toLowerCase().indexOf("DayLight".toLowerCase()) !== -1) {
        return true;
    }
    else return false;
}

console.log(isDaylightsSavings( new Date("6/20/2018")));
console.log(isDaylightsSavings( new Date("12/20/2018")));
console.log(isDaylightsSavings( new Date("3/20/2018")));
console.log(isDaylightsSavings( new Date("3/12/2018")));
console.log(isDaylightsSavings( new Date("3/11/2018")));

function greatestCommonDivisor(x, y) {
    for (let i = x ; i >= 1; i--) {
        if (x % i == 0) {
            if (y % i == 0) return i;
        }
    }
}
console.log(greatestCommonDivisor(2, 4));
console.log(greatestCommonDivisor(3, 4));
console.log(greatestCommonDivisor(3, 5));
console.log(greatestCommonDivisor(3, 6));

function sortByAge(arr) {


    return true;
}

var users = [
    {name: "Mike", age: 25},
    {name: "Jhon", age: 20},
    {name: "Dan", age: 22}
];

var result = sortByAge(users);

// result will be equal to
//
//     [
//     {name: "Jhon", age: 20},
//         {name: "Dan", age: 22},
//         {name: "Mike", age: 25}
//     ];
