function isNatural(n) {
    if (isNaN(Number(n)) || Number(n) < 0 || Number(n) % 1 != 0) {
        return false;
    }
    return true;
}
console.log(isNatural(5));
console.log(isNatural(5.4));
console.log(isNatural(5.0));
console.log(isNatural('57'));
console.log(isNatural('57.56'));
console.log(isNatural('57.0'));
console.log(isNatural('s57'));
console.log(isNatural('-5'));

