function sumNumbers(arr){
    let sum = 0;
    for (let i in arr) {
        let curArrElement = Number(arr[i]);
        if (!isNaN(curArrElement)) {
            sum += curArrElement;
        }
    }
    return sum;
}
function roundNumber(number, digits) {
    if (parseFloat(number).toFixed(digits) % 1 != 0) {
        return parseFloat(number).toFixed(digits);
    }
    else return parseFloat(number).toFixed();
}

function roundToLargest(number) {
    if (number % 1 > 0) {
        return parseInt(number) + 1;
    }
    else return number;
}


console.log(sumNumbers([1, "2", "abc", 3]));
console.log(sumNumbers([1, -33, "abc", 3]));
console.log(roundNumber(123.59, 1));
console.log(roundNumber(123.123123, 2));
console.log(roundNumber(21, 2));
console.log(roundToLargest(2.5));
console.log(roundToLargest(3.1));
console.log(roundToLargest(3.0));
