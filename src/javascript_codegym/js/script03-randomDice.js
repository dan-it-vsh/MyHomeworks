//Create a function that will return a random integer between 1 and 6. 
//Use Math.random to generate a random number.
//
//For example, randomDice() may return 3 (but number needs to be 
//random every time, not preset).
function randomDice() {
    return Math.floor(Math.random() * 7);
}
console.log(randomDice());
console.log(randomDice());
console.log(randomDice());
