//Given the function that receives two arguments - number A and number B, 
//add them and return the result. However, number A and number B can be a String 
//representing a number - in that case, you need to cast them to a numeric value 
//before you will add them. If one or both of provided values are not a Number and 
//not a String that can be cast to a number - return false instead.
//
//For example, addNumbers("2", 2) will return a number 4.
//

function addNumbers(numA, numB) {
    if (isNaN(Number(numA)) || isNaN(Number(numB))) {
        return false;                            
    }
    return +numA + +numB;
}
console.log(addNumbers(10, 20));
console.log(addNumbers('10', '20'));
console.log(addNumbers(10, 'd20'));
    
