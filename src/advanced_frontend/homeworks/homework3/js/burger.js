/**
 * Класс, объекты которого описывают параметры гамбургера.
 *
 * @constructor
 * @param size        Размер
 * @param stuffing    Начинка
 * @throws {HamburgerException}  При неправильном использовании
 */

function Hamburger(size, stuffing) {
    this.size = size;
    this.stuffing = stuffing;
    this.toppings = [];

    if (size !== Hamburger.SIZE_LARGE && size !== Hamburger.SIZE_SMALL) {
        throw new HamburgerException(size + ' - incorrect size argument');
    }

    if (stuffing !== Hamburger.STUFFING_CHEESE
        && stuffing !== Hamburger.STUFFING_POTATO
        && stuffing !== Hamburger.STUFFING_SALAD) {
        throw new HamburgerException(stuffing + ' - incorrect stuffing argument');
    }
}

/* Размеры, виды начинок и добавок */
Hamburger.SIZE_SMALL = "small";
Hamburger.SIZE_LARGE = "large";
Hamburger.STUFFING_CHEESE = "cheese";
Hamburger.STUFFING_SALAD = "salad";
Hamburger.STUFFING_POTATO = "potato";
Hamburger.TOPPING_MAYO = "mayo";
Hamburger.TOPPING_SPICE = "spice";

/**
 * Добавить добавку к гамбургеру. Можно добавить несколько
 * добавок, при условии, что они разные.
 *
 * @param topping     Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */

Hamburger.prototype.addTopping = function (topping) {

    if (topping !== Hamburger.TOPPING_MAYO && topping !== Hamburger.TOPPING_SPICE)
        throw new HamburgerException(topping + ' - incorrect topping argument');

    if (~(this.toppings.indexOf(topping))) {
        throw new HamburgerException('This topping already exists in the order');
    } else this.toppings.push(topping);

};

/**
 * Убрать добавку, при условии, что она ранее была
 * добавлена.
 *
 * @param topping   Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */

Hamburger.prototype.removeTopping = function (topping) {
    let removedTopping = this.toppings.indexOf(topping);
    if (~removedTopping) {
        this.toppings.splice(removedTopping, 1);
    } else
        throw new HamburgerException('Topping ' + topping + ' does not exist in the order');
};

/**
 * Получить список добавок.
 *
 * @return {Array} Массив добавленных добавок, содержит константы
 *                 Hamburger.TOPPING_*
 */

Hamburger.prototype.getToppings = function () {
    return this.toppings;
};

/* Узнать размер гамбургера */

Hamburger.prototype.getSize = function () {
    return this.size;
};

/* Узнать начинку гамбургера */

Hamburger.prototype.getStuffing = function () {
    return this.stuffing;
};

/* Узнать цену гамбургера @return {Number} Цена в тугриках */

Hamburger.prototype.calculatePrice = function () {

    var price = 0;
    if (this.size === Hamburger.SIZE_SMALL) {
        price += 50;
    } else {
        price += 100;
    }

    if (this.stuffing === Hamburger.STUFFING_CHEESE) {
        price += 10;
    } else if (this.stuffing === Hamburger.STUFFING_SALAD) {
        price += 20;
    } else {
        price += 15;
    }

    if (~(this.toppings.indexOf(Hamburger.TOPPING_SPICE))) {
        price += 15;
    }

    if (~(this.toppings.indexOf(Hamburger.TOPPING_MAYO))) {
        price += 20;
    }

    return price;
};

/* Узнать калорийность @return {Number} Калорийность в калориях */

Hamburger.prototype.calculateCalories = function () {
    var calories = 0;
    if (this.size === Hamburger.SIZE_SMALL) {
        calories += 20;
    } else {
        calories += 40;
    }

    if (this.stuffing === Hamburger.STUFFING_CHEESE) {
        calories += 20;
    } else if (this.stuffing === Hamburger.STUFFING_SALAD) {
        calories += 5;
    } else {
        calories += 10;
    }

    if (~(this.toppings.indexOf(Hamburger.TOPPING_MAYO))) {
        calories += 5;
    }

    return calories;
};

/* Представляет информацию об ошибке в ходе работы с гамбургером.
* Подробности хранятся в свойстве message.
* @constructor
*/

function HamburgerException(errorMessage) {
    this.name = 'HamburgerException';
    this.message = errorMessage;
}

// маленький гамбургер с начинкой из сыра
var hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
// добавка из майонеза
hamburger.addTopping(Hamburger.TOPPING_MAYO);
// спросим сколько там калорий
console.log("Calories: %f", hamburger.calculateCalories());
// сколько стоит
console.log("Price: %f", hamburger.calculatePrice());
// я тут передумал и решил добавить еще приправу
hamburger.addTopping(Hamburger.TOPPING_SPICE);
// А сколько теперь стоит?
console.log("Price with spice: %f", hamburger.calculatePrice());
// Проверить, большой ли гамбургер?
console.log("Is hamburger large ? : %s", hamburger.getSize() === Hamburger.SIZE_LARGE); // -> false
// Убрать добавку
hamburger.removeTopping(Hamburger.TOPPING_SPICE);
console.log("Have %d toppings", hamburger.getToppings().length); // 1