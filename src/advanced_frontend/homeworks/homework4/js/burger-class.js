'use strict';

class Hamburger {
    constructor(size, stuffing) {
        this.toppings = [];
        this.setSize(size);
        this.setStuffing(stuffing);
    }

    setSize(size) {
        if (size !== Hamburger.SIZE_LARGE && size !== Hamburger.SIZE_SMALL) {
            throw new HamburgerException(size + ' - incorrect size argument');
        } else {
            this.size = size;
        }
    }

    setStuffing(stuffing) {
        if (stuffing !== Hamburger.STUFFING_CHEESE
            && stuffing !== Hamburger.STUFFING_POTATO
            && stuffing !== Hamburger.STUFFING_SALAD) {
            throw new HamburgerException(stuffing + ' - incorrect stuffing argument');
        }
        else {
            this.stuffing = stuffing;
        }
    }

    addTopping(topping) {

        if (topping !== Hamburger.TOPPING_MAYO && topping !== Hamburger.TOPPING_SPICE)
            throw new HamburgerException(topping + ' - incorrect topping argument');

        if (~(this.toppings.indexOf(topping))) {
            throw new HamburgerException('Topping ' + topping + ' already exists in the order');
        } else this.toppings.push(topping);

    }

    removeTopping(topping) {
        let removedTopping = this.toppings.indexOf(topping);
        if (~removedTopping) {
            this.toppings.splice(removedTopping, 1);
        } else
            throw new HamburgerException('Topping ' + topping + ' does not exist in the order');
    }

    get getToppings() {
        return this.toppings;
    }


    get getSize() {
        return this.size;
    }


    get getStuffing() {
        return this.stuffing;
    }

    calculatePrice() {

        let price = 0;
        if (this.size === Hamburger.SIZE_SMALL) {
            price += 50;
        } else {
            price += 100;
        }

        if (this.stuffing === Hamburger.STUFFING_CHEESE) {
            price += 10;
        } else if (this.stuffing === Hamburger.STUFFING_SALAD) {
            price += 20;
        } else {
            price += 15;
        }

        if (~(this.toppings.indexOf(Hamburger.TOPPING_SPICE))) {
            price += 15;
        }

        if (~(this.toppings.indexOf(Hamburger.TOPPING_MAYO))) {
            price += 20;
        }

        return price;
    }

    calculateCalories() {
        let calories = 0;
        if (this.size === Hamburger.SIZE_SMALL) {
            calories += 20;
        } else {
            calories += 40;
        }

        if (this.stuffing === Hamburger.STUFFING_CHEESE) {
            calories += 20;
        } else if (this.stuffing === Hamburger.STUFFING_SALAD) {
            calories += 5;
        } else {
            calories += 10;
        }

        if (~(this.toppings.indexOf(Hamburger.TOPPING_MAYO))) {
            calories += 5;
        }

        return calories;
    }

}

function HamburgerException(errorMessage) {
    this.name = 'HamburgerException';
    this.message = errorMessage;
}


/* Размеры, виды начинок и добавок */
Hamburger.SIZE_SMALL = "small";
Hamburger.SIZE_LARGE = "large";
Hamburger.STUFFING_CHEESE = "cheese";
Hamburger.STUFFING_SALAD = "salad";
Hamburger.STUFFING_POTATO = "potato";
Hamburger.TOPPING_MAYO = "mayo";
Hamburger.TOPPING_SPICE = "spice";


// маленький гамбургер с начинкой из сыра
let hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
// добавка из майонеза
hamburger.addTopping(Hamburger.TOPPING_MAYO);
// спросим сколько там калорий
console.log("Calories: %f", hamburger.calculateCalories());
// сколько стоит
console.log("Price: %f", hamburger.calculatePrice());
// я тут передумал и решил добавить еще приправу
hamburger.addTopping(Hamburger.TOPPING_SPICE);
console.log("Have %d toppings", hamburger.getToppings.length); // 2
// А сколько теперь стоит?
console.log("Price with spice: %f", hamburger.calculatePrice());
// Проверить, большой ли гамбургер?
console.log("Is hamburger large ? : %s", hamburger.getSize === Hamburger.SIZE_LARGE); // -> false
// Убрать добавку
hamburger.removeTopping(Hamburger.TOPPING_SPICE);
console.log("Have %d toppings", hamburger.getToppings.length); // 1