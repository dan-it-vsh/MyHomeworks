function addTable() {
    let style = document.createElement('style');
    style.type = 'text/css';
    style.innerHTML = '.body { background-color: #bbbbbb; }';
    style.innerHTML += '.table { border-collapse: collapse; margin: 0 auto }';
    style.innerHTML += '.table-cell { width: 1vw; height: 1vw; border: 0.2vw solid #555555; }';
    style.innerHTML += '.table-cell-white { background-color: #fff; }';
    style.innerHTML += '.table-cell-black { background-color: #000; }';

    document.getElementsByTagName('head')[0].appendChild(style);

    let element = document.querySelector('body');
    element.classList.add('body');
    const TBL_SIZE_HORIZONTAL = 30;
    const TBL_SIZE_VERTICAL = 30;

    let tbl = document.createElement('table');

    for (i = 0; i < TBL_SIZE_VERTICAL; i++) {
        let row = document.createElement('tr');
        for (j = 0; j < TBL_SIZE_HORIZONTAL; j++) {
            let col = document.createElement('td');
            col.classList.add('table-cell', 'table-cell-white');
            row.appendChild(col);
        }
        tbl.appendChild(row);
    }
    element.appendChild(tbl);
    tbl.classList.add('table');
}

document.addEventListener("click", function () {
    let elemList = document.querySelectorAll(':hover');

    // if the click is inside the table nodeList.length >= 5 : html, body, table, tr, td, ...
    // toggle class for only 'TD' that is hovered

    if (elemList.length > 2) { // if nodeList.legth is >= 5: html. body,
        [].forEach.call(elemList, function (elem) {
            if (elem.nodeName === 'TD') {
                elem.classList.toggle('table-cell-white');
                elem.classList.toggle('table-cell-black');
            }
        });

    } else {

        // if the click is outside the table nodeList.length == 2 : html, body
        // collect all elements 'TD' and toggle the class

        let cellList = document.querySelectorAll('TD');
        [].forEach.call(cellList, function (cell) {
            cell.classList.toggle('table-cell-white');
            cell.classList.toggle('table-cell-black');
        });
    }
});

addTable();
