filterSelection("all"); // Execute the function and show all columns
function filterSelection(c) {
    var x, i;
    x = document.getElementsByClassName("section-work-grid-column");
    if (c === "all") c = "";
    // Add the "show" class (display:block) to the filtered elements, and remove the "show" class from the elements that are not selected
    for (i = 0; i < x.length; i++) {
        funcRemoveClass(x[i], "section-work-grid-elements-filtered-show");
        if (x[i].className.indexOf(c) > -1) funcAddClass(x[i], "section-work-grid-elements-filtered-show");
    }
}

// Show filtered elements
function funcAddClass(element, name) {
    var i, arr1, arr2;
    arr1 = element.className.split(" ");
    arr2 = name.split(" ");
    for (i = 0; i < arr2.length; i++) {
        if (arr1.indexOf(arr2[i]) === -1) {
            element.className += " " + arr2[i];
        }
    }
}

// Hide elements that are not selected
function funcRemoveClass(element, name) {
    var i, arr1, arr2;
    arr1 = element.className.split(" ");
    arr2 = name.split(" ");
    for (i = 0; i < arr2.length; i++) {
        while (arr1.indexOf(arr2[i]) > -1) {
            arr1.splice(arr1.indexOf(arr2[i]), 1);
        }
    }
    element.className = arr1.join(" ");
}

// Add active class to the current button (highlight it)
let btnContainer = document.getElementById("sectionWorkButtonContainer");
let btns = btnContainer.getElementsByClassName("section-work-menu-btn");
for (let i = 0; i < btns.length; i++) {
    btns[i].addEventListener("click", function() {
        let current = document.getElementsByClassName("active");
        current[0].className = current[0].className.replace(" active", "");
        // current.className = current.className.replace(" active", "");
        this.className += " active";
    });
}