// When the DOM is ready, run this function
$(document).ready(function () {
    //Set the carousel options
    $('.carousel').carousel({
        interval: false
    });
});